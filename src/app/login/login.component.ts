import { Component } from "@angular/core";
import { UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm: UntypedFormGroup;

  constructor(private router: Router) {
    this.loginForm = new UntypedFormGroup({
      email: new UntypedFormControl("",[Validators.required, Validators.pattern("[^ @]*@[^ @]*")]),
      password: new UntypedFormControl("", [Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$")]), 
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const email = this.loginForm.value.email;
      const password = this.loginForm.value.password;

      // Perform login logic here
      // You can make API calls or any other authentication mechanism
      console.log('Username:', email);
      console.log('Password:', password);
      // Redirect to the dashboard page
    }
    this.router.navigate(['/dashboard']);
  }
}
