import { Component } from "@angular/core";
import { FormBuilder, FormGroup, UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  signupForm: UntypedFormGroup;
  image: string = "";
  selectedFiles?: FileList;

  constructor(private router: Router) {
    this.signupForm = new UntypedFormGroup({
      email: new UntypedFormControl("",[Validators.required, Validators.pattern("[^ @]*@[^ @]*")]),
      password: new UntypedFormControl("", [Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$")]), 
      confirmpassword: new UntypedFormControl("", [Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$")]),
      name: new UntypedFormControl("", [Validators.required]),
      prefix: new UntypedFormControl({value: '+351', disabled: true} ),
      phone: new UntypedFormControl("", [Validators.required, Validators.pattern('[- +()0-9]+'), Validators.maxLength(9)]),
      description: new UntypedFormControl("", [Validators.required]),
      image: new UntypedFormControl("", [Validators.required])
    });
  }

  get f() { return this.signupForm.controls; }

  onSubmit() {
    if (this.signupForm.valid) {
      const formData = this.signupForm.value;

      // Perform signup logic here
      // You can make API calls or any other registration mechanism
      console.log('Signup Form Data:', formData);
      
      // Redirect to the dashboard page
    }
    this.router.navigate(['/dashboard']);
  }

  selectFiles(event: any): void {
    this.selectedFiles = event.target.files;

    if (this.selectedFiles && this.selectedFiles[0]) {
      const numberOfFiles = this.selectedFiles.length;
      for (let i = 0; i < numberOfFiles; i++) {
        const reader = new FileReader();

        reader.onload = (e: any) => {
          this.image = e.target.result;
        };

        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }
  }
}
